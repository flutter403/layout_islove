import 'package:flutter/material.dart';

class MediaQueryApp extends StatelessWidget {
  final double size;

  const MediaQueryApp({Key? key, required this.size}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    return Container(
      child: screenHeight > 400
          ? Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                FlutterLogo(size: size),
                Icon(Icons.favorite, color: Colors.red, size: size),
              ],
            )
          : Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                FlutterLogo(size: size),
                Icon(Icons.favorite, color: Colors.red, size: size),
              ],
            ),
    );
  }
}
