import 'package:flutter/material.dart';

class LayoutBuilderApp extends StatelessWidget {
  final double size;

  const LayoutBuilderApp({Key? key, required this.size}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      final maxHeight = constraints.maxHeight > 400;
      return maxHeight
          ? Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                FlutterLogo(size: size),
                Icon(Icons.favorite, color: Colors.red, size: size),
              ],
            )
          : Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                FlutterLogo(size: size),
                Icon(Icons.favorite, color: Colors.red, size: size),
              ],
            );
    });
  }
}
